import torch as th

from sknrf.enums.device import Instrument
from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.enums.signal import transform_color_map
from sknrf.settings import Settings
from sknrf.model.transform.tf.base import TFTransform
from sknrf.utilities.numeric import Info, bounded_property
from sknrf.widget.propertybrowser.view import Domain


class ReceiverIQOffsetTransform(TFTransform):
    """Simple IQ Offset Transform that calculates the IQ Offset based on the last measurement.

    This algorithm calculates the IQ Offset based on the average offset of all samples.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : tuple
        Port numbers that apply the transform.

    """

    _num_ports = 1
    _domain = Domain.TF
    _device = "cpu"
    _preview_filename = ":/PNG/t_circuit_transform.png"
    _default_filename = ""
    _color_code = Settings().color_map[transform_color_map[Domain.TF]]
    display_order = ["name", "ports", "instrument_flags"]
    optimize = False

    def __new__(cls, name: str = "IQ Offset", ports: tuple = (1,), instrument_flags=Instrument.LFRECEIVER,
                data: th.Tensor = None,  offsets: th.Tensor = th.tensor([0.0])):
        self = super(ReceiverIQOffsetTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                             data=data)
        self._offsets = th.zeros((Settings().f_points, 2), dtype=si_dtype_map[SI.V])
        return self

    def __getnewargs__(self):
        state = super(ReceiverIQOffsetTransform, self).__getnewargs__()
        state = tuple(list(state) +
                      [self._offsets])
        return state

    def __init__(self, name: str = "IQ Offset", ports: tuple = (1,), instrument_flags=Instrument.LFRECEIVER,
                 data: th.Tensor = None, offsets: th.Tensor = th.tensor([0.0])):
        super(ReceiverIQOffsetTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                        data=data)

        self.__info__()

        # Initialize object PROPERTIES
        self.set_data(offsets)

    def __getstate__(self, state={}):
        super(ReceiverIQOffsetTransform, self).__getstate__(state)
        state["offsets"] = self.offsets
        return state

    def __setstate__(self, state):
        super(ReceiverIQOffsetTransform, self).__setstate__(state)

        self.__info__()

        # Initialize object PROPERTIES
        self._offsets = state["offsets"]

    def __info__(self):
        super(ReceiverIQOffsetTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["offsets"] = Info("offsets", read=False, write=False, check=False)

    @bounded_property
    def offsets(self):
        return self._offsets

    @offsets.setter
    def offsets(self, offsets):
        self._offsets = offsets

    def set_data(self, data):
        devices = self.device_model()
        port_index = self.ports[0]
        if self.instrument_flags & Instrument.LFRECEIVER:
            self._offsets[0:1, 0] = devices.ports[port_index].lfreceiver.v.mean(dim=-2)
            self._offsets[0:1, 1] = devices.ports[port_index].lfreceiver.i.mean(dim=-2)

    def get_stimulus(self, *args):
        return args

    def set_stimulus(self, *args):
        return args

    def get_response(self, *args):
        p = self.ports[0]
        v, i, z = args[p][0], args[p][1], args[p][2]
        s = v.shape

        offset = self._offsets
        if self.instrument_flags & Instrument.RESPONSE:
            vi = th.stack((v, i), dim=-1)
            vi = vi - offset
            v, i = th.split(vi, 1, dim=-1)

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z.reshape(s)
        return tuple(res)
